﻿namespace ChildrenCalc
{
    partial class Example
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Example));
            this.panel1 = new System.Windows.Forms.Panel();
            this.rightLabel = new System.Windows.Forms.Label();
            this.wrongLabel1 = new System.Windows.Forms.Label();
            this.wrongLabel2 = new System.Windows.Forms.Label();
            this.digit9 = new System.Windows.Forms.PictureBox();
            this.digit8 = new System.Windows.Forms.PictureBox();
            this.digit7 = new System.Windows.Forms.PictureBox();
            this.digit6 = new System.Windows.Forms.PictureBox();
            this.digit5 = new System.Windows.Forms.PictureBox();
            this.digit4 = new System.Windows.Forms.PictureBox();
            this.digit3 = new System.Windows.Forms.PictureBox();
            this.digit2 = new System.Windows.Forms.PictureBox();
            this.digit1 = new System.Windows.Forms.PictureBox();
            this.digit0 = new System.Windows.Forms.PictureBox();
            this.exitPictureBox = new System.Windows.Forms.PictureBox();
            this.firstRandomDigit = new System.Windows.Forms.PictureBox();
            this.centerOutput = new System.Windows.Forms.PictureBox();
            this.rightOutput = new System.Windows.Forms.PictureBox();
            this.leftOutput = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.secondRandomDigit = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.nextButton = new ChildrenCalc.RoundButton();
            this.cleanButton = new ChildrenCalc.RoundButton();
            this.endTestButton = new ChildrenCalc.RoundButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.digit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstRandomDigit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.centerOutput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightOutput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftOutput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondRandomDigit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.centerOutput);
            this.panel1.Controls.Add(this.rightOutput);
            this.panel1.Controls.Add(this.leftOutput);
            this.panel1.Controls.Add(this.pictureBox17);
            this.panel1.Controls.Add(this.pictureBox18);
            this.panel1.Controls.Add(this.secondRandomDigit);
            this.panel1.Location = new System.Drawing.Point(26, 98);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(580, 146);
            this.panel1.TabIndex = 16;
            // 
            // rightLabel
            // 
            this.rightLabel.AutoSize = true;
            this.rightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rightLabel.ForeColor = System.Drawing.Color.LawnGreen;
            this.rightLabel.Location = new System.Drawing.Point(629, 402);
            this.rightLabel.Name = "rightLabel";
            this.rightLabel.Size = new System.Drawing.Size(108, 20);
            this.rightLabel.TabIndex = 55;
            this.rightLabel.Text = "Правильно!";
            this.rightLabel.Visible = false;
            // 
            // wrongLabel1
            // 
            this.wrongLabel1.AutoSize = true;
            this.wrongLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.wrongLabel1.ForeColor = System.Drawing.Color.Red;
            this.wrongLabel1.Location = new System.Drawing.Point(649, 357);
            this.wrongLabel1.Name = "wrongLabel1";
            this.wrongLabel1.Size = new System.Drawing.Size(75, 20);
            this.wrongLabel1.TabIndex = 56;
            this.wrongLabel1.Text = "Ошибка";
            this.wrongLabel1.Visible = false;
            // 
            // wrongLabel2
            // 
            this.wrongLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.wrongLabel2.ForeColor = System.Drawing.Color.Red;
            this.wrongLabel2.Location = new System.Drawing.Point(597, 377);
            this.wrongLabel2.Name = "wrongLabel2";
            this.wrongLabel2.Size = new System.Drawing.Size(178, 45);
            this.wrongLabel2.TabIndex = 57;
            this.wrongLabel2.Text = "Правильный ответ: 40";
            this.wrongLabel2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.wrongLabel2.Visible = false;
            // 
            // digit9
            // 
            this.digit9.Image = global::ChildrenCalc.Properties.Resources._9;
            this.digit9.Location = new System.Drawing.Point(485, 386);
            this.digit9.Name = "digit9";
            this.digit9.Size = new System.Drawing.Size(108, 121);
            this.digit9.TabIndex = 54;
            this.digit9.TabStop = false;
            // 
            // digit8
            // 
            this.digit8.Image = global::ChildrenCalc.Properties.Resources._8;
            this.digit8.Location = new System.Drawing.Point(371, 386);
            this.digit8.Name = "digit8";
            this.digit8.Size = new System.Drawing.Size(108, 121);
            this.digit8.TabIndex = 53;
            this.digit8.TabStop = false;
            // 
            // digit7
            // 
            this.digit7.Image = global::ChildrenCalc.Properties.Resources._7;
            this.digit7.Location = new System.Drawing.Point(257, 386);
            this.digit7.Name = "digit7";
            this.digit7.Size = new System.Drawing.Size(108, 121);
            this.digit7.TabIndex = 52;
            this.digit7.TabStop = false;
            // 
            // digit6
            // 
            this.digit6.Image = global::ChildrenCalc.Properties.Resources._6;
            this.digit6.Location = new System.Drawing.Point(143, 386);
            this.digit6.Name = "digit6";
            this.digit6.Size = new System.Drawing.Size(108, 121);
            this.digit6.TabIndex = 51;
            this.digit6.TabStop = false;
            // 
            // digit5
            // 
            this.digit5.Image = global::ChildrenCalc.Properties.Resources._5;
            this.digit5.Location = new System.Drawing.Point(29, 386);
            this.digit5.Name = "digit5";
            this.digit5.Size = new System.Drawing.Size(108, 121);
            this.digit5.TabIndex = 50;
            this.digit5.TabStop = false;
            // 
            // digit4
            // 
            this.digit4.Image = global::ChildrenCalc.Properties.Resources._4;
            this.digit4.Location = new System.Drawing.Point(485, 263);
            this.digit4.Name = "digit4";
            this.digit4.Size = new System.Drawing.Size(108, 121);
            this.digit4.TabIndex = 49;
            this.digit4.TabStop = false;
            // 
            // digit3
            // 
            this.digit3.Image = global::ChildrenCalc.Properties.Resources._3;
            this.digit3.Location = new System.Drawing.Point(371, 263);
            this.digit3.Name = "digit3";
            this.digit3.Size = new System.Drawing.Size(108, 121);
            this.digit3.TabIndex = 48;
            this.digit3.TabStop = false;
            // 
            // digit2
            // 
            this.digit2.Image = global::ChildrenCalc.Properties.Resources._2;
            this.digit2.Location = new System.Drawing.Point(257, 263);
            this.digit2.Name = "digit2";
            this.digit2.Size = new System.Drawing.Size(108, 121);
            this.digit2.TabIndex = 47;
            this.digit2.TabStop = false;
            // 
            // digit1
            // 
            this.digit1.Image = global::ChildrenCalc.Properties.Resources._1;
            this.digit1.Location = new System.Drawing.Point(143, 263);
            this.digit1.Name = "digit1";
            this.digit1.Size = new System.Drawing.Size(108, 121);
            this.digit1.TabIndex = 46;
            this.digit1.TabStop = false;
            // 
            // digit0
            // 
            this.digit0.Image = ((System.Drawing.Image)(resources.GetObject("digit0.Image")));
            this.digit0.Location = new System.Drawing.Point(29, 263);
            this.digit0.Name = "digit0";
            this.digit0.Size = new System.Drawing.Size(108, 121);
            this.digit0.TabIndex = 45;
            this.digit0.TabStop = false;
            // 
            // exitPictureBox
            // 
            this.exitPictureBox.Image = global::ChildrenCalc.Properties.Resources.free_icon_cancel_258348;
            this.exitPictureBox.Location = new System.Drawing.Point(737, 12);
            this.exitPictureBox.Name = "exitPictureBox";
            this.exitPictureBox.Size = new System.Drawing.Size(20, 20);
            this.exitPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.exitPictureBox.TabIndex = 42;
            this.exitPictureBox.TabStop = false;
            // 
            // firstRandomDigit
            // 
            this.firstRandomDigit.Image = global::ChildrenCalc.Properties.Resources._5;
            this.firstRandomDigit.Location = new System.Drawing.Point(43, 114);
            this.firstRandomDigit.Name = "firstRandomDigit";
            this.firstRandomDigit.Size = new System.Drawing.Size(108, 121);
            this.firstRandomDigit.TabIndex = 30;
            this.firstRandomDigit.TabStop = false;
            // 
            // centerOutput
            // 
            this.centerOutput.Location = new System.Drawing.Point(410, 14);
            this.centerOutput.Name = "centerOutput";
            this.centerOutput.Size = new System.Drawing.Size(108, 121);
            this.centerOutput.TabIndex = 33;
            this.centerOutput.TabStop = false;
            // 
            // rightOutput
            // 
            this.rightOutput.Location = new System.Drawing.Point(462, 14);
            this.rightOutput.Name = "rightOutput";
            this.rightOutput.Size = new System.Drawing.Size(108, 121);
            this.rightOutput.TabIndex = 35;
            this.rightOutput.TabStop = false;
            // 
            // leftOutput
            // 
            this.leftOutput.Location = new System.Drawing.Point(367, 14);
            this.leftOutput.Name = "leftOutput";
            this.leftOutput.Size = new System.Drawing.Size(108, 121);
            this.leftOutput.TabIndex = 34;
            this.leftOutput.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::ChildrenCalc.Properties.Resources.multi;
            this.pictureBox17.Location = new System.Drawing.Point(129, 55);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(56, 53);
            this.pictureBox17.TabIndex = 31;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = global::ChildrenCalc.Properties.Resources.equal;
            this.pictureBox18.Location = new System.Drawing.Point(303, 55);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(56, 51);
            this.pictureBox18.TabIndex = 32;
            this.pictureBox18.TabStop = false;
            // 
            // secondRandomDigit
            // 
            this.secondRandomDigit.Image = global::ChildrenCalc.Properties.Resources._8;
            this.secondRandomDigit.Location = new System.Drawing.Point(189, 14);
            this.secondRandomDigit.Name = "secondRandomDigit";
            this.secondRandomDigit.Size = new System.Drawing.Size(108, 121);
            this.secondRandomDigit.TabIndex = 29;
            this.secondRandomDigit.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ChildrenCalc.Properties.Resources.ForMockup;
            this.pictureBox1.Location = new System.Drawing.Point(43, -98);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(609, 206);
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // nextButton
            // 
            this.nextButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(174)))), ((int)(((byte)(174)))));
            this.nextButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(174)))), ((int)(((byte)(174)))));
            this.nextButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.nextButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.nextButton.Location = new System.Drawing.Point(625, 425);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(117, 35);
            this.nextButton.TabIndex = 44;
            this.nextButton.Text = "ДАЛЕЕ";
            // 
            // cleanButton
            // 
            this.cleanButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(174)))), ((int)(((byte)(174)))));
            this.cleanButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(174)))), ((int)(((byte)(174)))));
            this.cleanButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.cleanButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.cleanButton.Location = new System.Drawing.Point(625, 114);
            this.cleanButton.Name = "cleanButton";
            this.cleanButton.Rounding = 20;
            this.cleanButton.Size = new System.Drawing.Size(99, 30);
            this.cleanButton.TabIndex = 43;
            this.cleanButton.Text = "ОЧИСТИТЬ";
            // 
            // endTestButton
            // 
            this.endTestButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(174)))), ((int)(((byte)(174)))));
            this.endTestButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(174)))), ((int)(((byte)(174)))));
            this.endTestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.endTestButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.endTestButton.Location = new System.Drawing.Point(625, 464);
            this.endTestButton.Name = "endTestButton";
            this.endTestButton.Size = new System.Drawing.Size(117, 35);
            this.endTestButton.TabIndex = 28;
            this.endTestButton.Text = "ЗАВЕРШИТЬ";
            // 
            // Example
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(134)))), ((int)(((byte)(134)))));
            this.ClientSize = new System.Drawing.Size(770, 531);
            this.Controls.Add(this.wrongLabel2);
            this.Controls.Add(this.wrongLabel1);
            this.Controls.Add(this.rightLabel);
            this.Controls.Add(this.digit9);
            this.Controls.Add(this.digit8);
            this.Controls.Add(this.digit7);
            this.Controls.Add(this.digit6);
            this.Controls.Add(this.digit5);
            this.Controls.Add(this.digit4);
            this.Controls.Add(this.digit3);
            this.Controls.Add(this.digit2);
            this.Controls.Add(this.digit1);
            this.Controls.Add(this.digit0);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.cleanButton);
            this.Controls.Add(this.exitPictureBox);
            this.Controls.Add(this.firstRandomDigit);
            this.Controls.Add(this.endTestButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Example";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ExampleForm";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.digit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstRandomDigit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.centerOutput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightOutput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftOutput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondRandomDigit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public RoundButton endTestButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.PictureBox secondRandomDigit;
        public System.Windows.Forms.PictureBox firstRandomDigit;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        public System.Windows.Forms.PictureBox exitPictureBox;
        public RoundButton cleanButton;
        public RoundButton nextButton;
        public System.Windows.Forms.PictureBox centerOutput;
        public System.Windows.Forms.PictureBox rightOutput;
        public System.Windows.Forms.PictureBox leftOutput;
        public System.Windows.Forms.PictureBox digit9;
        public System.Windows.Forms.PictureBox digit8;
        public System.Windows.Forms.PictureBox digit7;
        public System.Windows.Forms.PictureBox digit6;
        public System.Windows.Forms.PictureBox digit5;
        public System.Windows.Forms.PictureBox digit4;
        public System.Windows.Forms.PictureBox digit3;
        public System.Windows.Forms.PictureBox digit2;
        public System.Windows.Forms.PictureBox digit1;
        public System.Windows.Forms.PictureBox digit0;
        public System.Windows.Forms.Label rightLabel;
        public System.Windows.Forms.Label wrongLabel1;
        public System.Windows.Forms.Label wrongLabel2;
    }
}