﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace ChildrenCalc
{
    partial class QuantitySelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuantitySelector));
            this.panel1 = new System.Windows.Forms.Panel();
            this.centerOutput = new System.Windows.Forms.PictureBox();
            this.rightOutput = new System.Windows.Forms.PictureBox();
            this.leftOutput = new System.Windows.Forms.PictureBox();
            this.exitPictureBox = new System.Windows.Forms.PictureBox();
            this.digit9 = new System.Windows.Forms.PictureBox();
            this.digit8 = new System.Windows.Forms.PictureBox();
            this.digit7 = new System.Windows.Forms.PictureBox();
            this.digit6 = new System.Windows.Forms.PictureBox();
            this.digit5 = new System.Windows.Forms.PictureBox();
            this.digit4 = new System.Windows.Forms.PictureBox();
            this.digit3 = new System.Windows.Forms.PictureBox();
            this.digit2 = new System.Windows.Forms.PictureBox();
            this.digit1 = new System.Windows.Forms.PictureBox();
            this.digit0 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.nextButton = new ChildrenCalc.RoundButton();
            this.cleanButton = new ChildrenCalc.RoundButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.centerOutput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightOutput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftOutput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.centerOutput);
            this.panel1.Controls.Add(this.rightOutput);
            this.panel1.Controls.Add(this.leftOutput);
            this.panel1.Location = new System.Drawing.Point(224, 95);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(225, 130);
            this.panel1.TabIndex = 1;
            // 
            // centerOutput
            // 
            this.centerOutput.Location = new System.Drawing.Point(61, 3);
            this.centerOutput.Name = "centerOutput";
            this.centerOutput.Size = new System.Drawing.Size(108, 121);
            this.centerOutput.TabIndex = 15;
            this.centerOutput.TabStop = false;
            // 
            // rightOutput
            // 
            this.rightOutput.Location = new System.Drawing.Point(113, 3);
            this.rightOutput.Name = "rightOutput";
            this.rightOutput.Size = new System.Drawing.Size(108, 121);
            this.rightOutput.TabIndex = 16;
            this.rightOutput.TabStop = false;
            // 
            // leftOutput
            // 
            this.leftOutput.Location = new System.Drawing.Point(18, 3);
            this.leftOutput.Name = "leftOutput";
            this.leftOutput.Size = new System.Drawing.Size(108, 121);
            this.leftOutput.TabIndex = 15;
            this.leftOutput.TabStop = false;
            // 
            // exitPictureBox
            // 
            this.exitPictureBox.Image = global::ChildrenCalc.Properties.Resources.free_icon_cancel_258348;
            this.exitPictureBox.Location = new System.Drawing.Point(763, 12);
            this.exitPictureBox.Name = "exitPictureBox";
            this.exitPictureBox.Size = new System.Drawing.Size(20, 20);
            this.exitPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.exitPictureBox.TabIndex = 17;
            this.exitPictureBox.TabStop = false;
            // 
            // digit9
            // 
            this.digit9.Image = global::ChildrenCalc.Properties.Resources._9;
            this.digit9.Location = new System.Drawing.Point(515, 371);
            this.digit9.Name = "digit9";
            this.digit9.Size = new System.Drawing.Size(108, 121);
            this.digit9.TabIndex = 11;
            this.digit9.TabStop = false;
            // 
            // digit8
            // 
            this.digit8.Image = global::ChildrenCalc.Properties.Resources._8;
            this.digit8.Location = new System.Drawing.Point(401, 371);
            this.digit8.Name = "digit8";
            this.digit8.Size = new System.Drawing.Size(108, 121);
            this.digit8.TabIndex = 10;
            this.digit8.TabStop = false;
            // 
            // digit7
            // 
            this.digit7.Image = global::ChildrenCalc.Properties.Resources._7;
            this.digit7.Location = new System.Drawing.Point(287, 371);
            this.digit7.Name = "digit7";
            this.digit7.Size = new System.Drawing.Size(108, 121);
            this.digit7.TabIndex = 9;
            this.digit7.TabStop = false;
            // 
            // digit6
            // 
            this.digit6.Image = global::ChildrenCalc.Properties.Resources._6;
            this.digit6.Location = new System.Drawing.Point(173, 371);
            this.digit6.Name = "digit6";
            this.digit6.Size = new System.Drawing.Size(108, 121);
            this.digit6.TabIndex = 8;
            this.digit6.TabStop = false;
            // 
            // digit5
            // 
            this.digit5.Image = global::ChildrenCalc.Properties.Resources._5;
            this.digit5.Location = new System.Drawing.Point(59, 371);
            this.digit5.Name = "digit5";
            this.digit5.Size = new System.Drawing.Size(108, 121);
            this.digit5.TabIndex = 7;
            this.digit5.TabStop = false;
            // 
            // digit4
            // 
            this.digit4.Image = global::ChildrenCalc.Properties.Resources._4;
            this.digit4.Location = new System.Drawing.Point(515, 248);
            this.digit4.Name = "digit4";
            this.digit4.Size = new System.Drawing.Size(108, 121);
            this.digit4.TabIndex = 6;
            this.digit4.TabStop = false;
            // 
            // digit3
            // 
            this.digit3.Image = global::ChildrenCalc.Properties.Resources._3;
            this.digit3.Location = new System.Drawing.Point(401, 248);
            this.digit3.Name = "digit3";
            this.digit3.Size = new System.Drawing.Size(108, 121);
            this.digit3.TabIndex = 5;
            this.digit3.TabStop = false;
            // 
            // digit2
            // 
            this.digit2.Image = global::ChildrenCalc.Properties.Resources._2;
            this.digit2.Location = new System.Drawing.Point(287, 248);
            this.digit2.Name = "digit2";
            this.digit2.Size = new System.Drawing.Size(108, 121);
            this.digit2.TabIndex = 4;
            this.digit2.TabStop = false;
            // 
            // digit1
            // 
            this.digit1.Image = global::ChildrenCalc.Properties.Resources._1;
            this.digit1.Location = new System.Drawing.Point(173, 248);
            this.digit1.Name = "digit1";
            this.digit1.Size = new System.Drawing.Size(108, 121);
            this.digit1.TabIndex = 3;
            this.digit1.TabStop = false;
            // 
            // digit0
            // 
            this.digit0.Enabled = false;
            this.digit0.Image = ((System.Drawing.Image)(resources.GetObject("digit0.Image")));
            this.digit0.Location = new System.Drawing.Point(59, 248);
            this.digit0.Name = "digit0";
            this.digit0.Size = new System.Drawing.Size(108, 121);
            this.digit0.TabIndex = 2;
            this.digit0.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ChildrenCalc.Properties.Resources.image__5_;
            this.pictureBox1.Location = new System.Drawing.Point(33, -162);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(700, 235);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // nextButton
            // 
            this.nextButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(174)))), ((int)(((byte)(174)))));
            this.nextButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(174)))), ((int)(((byte)(174)))));
            this.nextButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.nextButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.nextButton.Location = new System.Drawing.Point(649, 456);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(117, 35);
            this.nextButton.TabIndex = 14;
            this.nextButton.Text = "ДАЛЕЕ";
            // 
            // cleanButton
            // 
            this.cleanButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(174)))), ((int)(((byte)(174)))));
            this.cleanButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(174)))), ((int)(((byte)(174)))));
            this.cleanButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.cleanButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.cleanButton.Location = new System.Drawing.Point(468, 95);
            this.cleanButton.Name = "cleanButton";
            this.cleanButton.Rounding = 20;
            this.cleanButton.Size = new System.Drawing.Size(99, 30);
            this.cleanButton.TabIndex = 13;
            this.cleanButton.Text = "ОЧИСТИТЬ";
            // 
            // QuantitySelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(134)))), ((int)(((byte)(134)))));
            this.ClientSize = new System.Drawing.Size(795, 503);
            this.Controls.Add(this.exitPictureBox);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.cleanButton);
            this.Controls.Add(this.digit9);
            this.Controls.Add(this.digit8);
            this.Controls.Add(this.digit7);
            this.Controls.Add(this.digit6);
            this.Controls.Add(this.digit5);
            this.Controls.Add(this.digit4);
            this.Controls.Add(this.digit3);
            this.Controls.Add(this.digit2);
            this.Controls.Add(this.digit1);
            this.Controls.Add(this.digit0);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "QuantitySelector";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GameForm";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.centerOutput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightOutput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftOutput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digit0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion

        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.PictureBox digit0;
        public System.Windows.Forms.PictureBox digit1;
        public System.Windows.Forms.PictureBox digit2;
        public System.Windows.Forms.PictureBox digit3;
        public System.Windows.Forms.PictureBox digit4;
        public System.Windows.Forms.PictureBox digit9;
        public System.Windows.Forms.PictureBox digit8;
        public System.Windows.Forms.PictureBox digit7;
        public System.Windows.Forms.PictureBox digit6;
        public System.Windows.Forms.PictureBox digit5;
        public System.Windows.Forms.PictureBox centerOutput;
        public System.Windows.Forms.PictureBox rightOutput;
        public System.Windows.Forms.PictureBox leftOutput;
        public RoundButton nextButton;
        public RoundButton cleanButton;
        public System.Windows.Forms.PictureBox exitPictureBox;
    }
}