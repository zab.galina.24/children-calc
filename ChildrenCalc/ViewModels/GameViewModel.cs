﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChildrenCalc.ViewModels
{
    public class GameViewModel
    {
        Random rnd = new Random();

        private MainMenu menu = new MainMenu();
        private QuantitySelector selector = new QuantitySelector();
        private Statistic statistic = new Statistic();
        private Example example = new Example();
        private Result result = new Result();

        private int examplesNumber = 0;
        private int exampleCount = 0;
        private int currentAnswer = 0;
        private int actualAnswer = 0;
        private int rightCount = 0;
        private int wrongCount = 0;

        public GameViewModel()
        {
            menu.Show();
            ConfigureMenuForm();
            ConfigureStatisticForm();
            ConfigureQuantitySelectorForm();
            ConfigureExampleForm();
            ConfigureResultForm();
        }

        private void ConfigureMenuForm()
        {
            menu.startGameButton.Click += new EventHandler(onStartGameButtonClick);
            menu.startMistakesButton.Click += new EventHandler(onStartMistakesButtonClick);
            menu.statisticButton.Click += new EventHandler(onStatisticButtonClick);
            menu.exitButton.Click += new EventHandler(onExitButtonClick);
        }

        private void ConfigureQuantitySelectorForm()
        {
            selector.exitPictureBox.Click += new EventHandler(onExitButtonClick);
            selector.nextButton.Click += new EventHandler(onGoToExampleFromSelectorClick);
            selector.cleanButton.Click += new EventHandler(onCleanOutputSelectorButtonClick);

            Dictionary<int, PictureBox> digits = GetDigitsFromSelector();
            foreach (PictureBox digit in digits.Values)
            {
                digit.Click += new EventHandler(onSelectorDigitClick);
            }
        }

        private void ConfigureExampleForm()
        {
            example.exitPictureBox.Click += new EventHandler(onExitButtonClick);
            example.nextButton.Click += new EventHandler(onGoToExampleFromExampleClick);
            example.cleanButton.Click += new EventHandler(onCleanOutputExampleButtonClick);
            example.endTestButton.Click += new EventHandler(onEndTestButtonClick);

            Dictionary<int, PictureBox> digits = GetDigitsFromExample();
            foreach (PictureBox digit in digits.Values)
            {
                digit.Click += new EventHandler(onExampleDigitClick);
            }
        }

        private void ConfigureResultForm()
        {
            result.exitPictureBox.Click += new EventHandler(onExitButtonClick);
            result.menuButton.Click += new EventHandler(onMenuButtonClick);
        }

        private void ConfigureStatisticForm()
        {

        }

        private void onStartGameButtonClick(object sender, EventArgs e)
        {
            menu.Hide();
            selector.Show();
        }

        private void onMenuButtonClick(object sender, EventArgs e)
        {
            result.Hide();
            menu.Show();
        }

        private void onStartMistakesButtonClick(object sender, EventArgs e)
        {
            menu.Hide();
            selector.Show();
        }

        private void onStatisticButtonClick(object sender, EventArgs e)
        {
            menu.Hide();
            statistic.Show();
        }

        private void onExitButtonClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void onCleanOutputSelectorButtonClick(object sender, EventArgs e)
        {
            selector.leftOutput.Image = null;
            selector.rightOutput.Image = null;
            selector.centerOutput.Image = null;
        }

        private void onCleanOutputExampleButtonClick(object sender, EventArgs e)
        {
            currentAnswer = 0;
            example.leftOutput.Image = null;
            example.rightOutput.Image = null;
            example.centerOutput.Image = null;
        }

        private void onSelectorDigitClick(object sender, EventArgs e)
        {
            Dictionary<int, PictureBox> digits = GetDigitsFromSelector();
            PictureBox target = sender as PictureBox;

            // Первая цифра введена
            if (selector.centerOutput.Image != null)
            {

                selector.leftOutput.Image = selector.centerOutput.Image;
                selector.centerOutput.Image = null;
                selector.centerOutput.Visible = false;
                selector.rightOutput.Image = target.Image;

                digits[0].Enabled = false;
            }
            // Цифры не введены
            else if (selector.leftOutput.Image == null && selector.centerOutput.Image == null)
            {
                selector.centerOutput.Visible = true;
                selector.centerOutput.Image = target.Image;

                digits[0].Enabled = true;
            }
        }

        private void onExampleDigitClick(object sender, EventArgs e)
        {
            Dictionary<int, PictureBox> digits = GetDigitsFromSelector();
            PictureBox target = sender as PictureBox;
            if (example.centerOutput.Image != null)
            {
                example.leftOutput.Image = example.centerOutput.Image;
                example.centerOutput.Image = null;
                example.centerOutput.Visible = false;
                example.rightOutput.Image = target.Image;

                digits[0].Enabled = false;
            }
            else if (example.leftOutput.Image == null && example.centerOutput.Image == null)
            {
                example.centerOutput.Visible = true;
                example.centerOutput.Image = target.Image;

                digits[0].Enabled = true;
            }
        }

        private void onGoToExampleFromSelectorClick(object sender, EventArgs e)
        {
            selector.Hide();

            Dictionary<int, PictureBox> digits = GetDigitsFromSelector();

            if (selector.leftOutput.Image != null)
            {
                examplesNumber = 10 * GetKeyByValue(digits, selector.leftOutput)
                    + GetKeyByValue(digits, selector.rightOutput);
            }
            else
            {
                examplesNumber = GetKeyByValue(digits, selector.centerOutput);
            }

            exampleCount = 0;
            CreateExample();
            example.Show();
        }

        private void onGoToExampleFromExampleClick(object sender, EventArgs e)
        {
            if (!example.wrongLabel1.Visible && !example.rightLabel.Visible)
            {
                Dictionary<int, PictureBox> digits = GetDigitsFromExample();

                if (example.leftOutput.Image != null)
                {
                    currentAnswer = 10 * GetKeyByValue(digits, example.leftOutput)
                        + GetKeyByValue(digits, example.rightOutput);
                }
                else
                {
                    currentAnswer = GetKeyByValue(digits, example.centerOutput);
                }

                if (currentAnswer == actualAnswer)
                {
                    rightCount++;
                    example.rightLabel.Visible = true;
                }
                else
                {
                    wrongCount++;
                    example.wrongLabel1.Visible = true;
                    example.wrongLabel2.Text = "Правильный ответ: " + actualAnswer.ToString();
                    example.wrongLabel2.Visible = true;
                }
            }
            else if (exampleCount == examplesNumber)
            {
                result.numberLabel.Text = examplesNumber.ToString();
                result.rightLabel.Text = rightCount.ToString();
                result.wrongLabel.Text = wrongCount.ToString();

                double rightPercent = rightCount / examplesNumber;
                if (rightPercent < 50)
                {
                    result.gradeLabel.Text = "2";
                }
                else if (rightPercent < 70)
                {
                    result.gradeLabel.Text = "3";
                }
                else if (rightPercent < 90)
                {
                    result.gradeLabel.Text = "4";
                }
                else if (rightPercent < 100)
                {
                    result.gradeLabel.Text = "5";
                }
                else
                {
                    result.gradeLabel.Text = "5+";
                }

                example.Hide();
                result.Show();
            }
            else
            {
                CreateExample();
                example.Show();
            }
        }

        private void onEndTestButtonClick(object sender, EventArgs e)
        {
            example.Hide();
            statistic.Show();
        }

        public void CreateExample()
        {
            example.leftOutput.Image = null;
            example.centerOutput.Image = null;
            example.rightOutput.Image = null;

            example.rightLabel.Visible = false;
            example.wrongLabel1.Visible = false;
            example.wrongLabel2.Visible = false;

            if (exampleCount != examplesNumber)
            {
                Dictionary<int, PictureBox> digit = GetDigitsFromExample();
                int firstDigit = rnd.Next(1, 9);
                int secondDigit = rnd.Next(1, 9);

                example.firstRandomDigit.Image = digit[firstDigit].Image;
                example.secondRandomDigit.Image = digit[secondDigit].Image;
                exampleCount++;

                actualAnswer = firstDigit * secondDigit;
            }
        }

        private Dictionary<int, PictureBox> GetDigitsFromSelector()
        {
            return new Dictionary<int, PictureBox>()
            {
                { 0, selector.digit0 },
                { 1, selector.digit1 },
                { 2, selector.digit2 },
                { 3, selector.digit3 },
                { 4, selector.digit4 },
                { 5, selector.digit5 },
                { 6, selector.digit6 },
                { 7, selector.digit7 },
                { 8, selector.digit8 },
                { 9, selector.digit9 },
            };
        }

        private Dictionary<int, PictureBox> GetDigitsFromExample()
        {
            return new Dictionary<int, PictureBox>()
            {
                { 0, example.digit0 },
                { 1, example.digit1 },
                { 2, example.digit2 },
                { 3, example.digit3 },
                { 4, example.digit4 },
                { 5, example.digit5 },
                { 6, example.digit6 },
                { 7, example.digit7 },
                { 8, example.digit8 },
                { 9, example.digit9 },
            };
        }

        private int GetKeyByValue(Dictionary<int, PictureBox> digits, PictureBox value)
        {
            foreach (var digit in digits)
            {
                if (value.Image == digit.Value.Image)
                    return digit.Key;
            }
            return -1;
        }
    }
}
